package main

import (
	"fmt"
	"gitlab.com/AndresFel/deploydatapower/util"
	"os"
)

const domain = "Deploy"
const ip = "192.168.99.100"
const port = "8001"
const user = "admin"
const pass = "admin"
const pathDeployment = "C:/Users/Abustos/Documents/deploydatapower/deployment/AWSFirma_DP.xml"
const pathService = "C:/Users/Abustos/Documents/deploydatapower/servicio/AWSFirma_MPG.zip"
const uri = "/mgmt/actionqueue/"
const nameDeploymentPolicy = "AWSFirma_DP"
const urlDeployment = "https://" + ip + ":" + port + uri + domain

func main() {
	step := os.Args[1]
	switch os := step; os {
	case "1":
		fmt.Println("Backup: ", os)
		host := "https://" + ip + ":" + port
		util.Backup(domain, host, user, pass)
	case "2":
		fmt.Println("Import Deployment: ", os)
		//"https://192.168.99.100:8001/mgmt/actionqueue/Deploy"
		util.ImportDeployment(pathDeployment, urlDeployment, user, pass)
	case "3":
		fmt.Println("Import Service: ", os)
		//time.Sleep(15000 * time.Millisecond)
		util.ImportService(pathService, urlDeployment, nameDeploymentPolicy, user, pass)
	case "4":
		fmt.Println("Save Config: ", os)
		//time.Sleep(5000 * time.Millisecond)
		//"https://192.168.99.100:8001/mgmt/actionqueue/"+domain
		util.SaveConfig(urlDeployment, user, pass)
	default:
		fmt.Println("No se reconoce comando ", os)
	}
}
