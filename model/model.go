package model

type ObjectType struct {
	Class        string `json:"class,omitempty"`
	Name         string `json:"name,omitempty"`
	Refobjects   string `json:"ref-objects,omitempty"`
	Reffiles     string `json:"ref-files,omitempty"`
	Includedebug string `json:"include-debug,omitempty"`
	Overwrite    string `json:"overwrite,omitempty"`
	ImportDebug  string `json:"importDebug,omitempty"`
}

type FileType struct {
	Name      string `json:"name,omitempty"`
	Overwrite string `json:"overwrite,omitempty"`
}

type DomainType struct {
	Name       string `json:"name"`
	Refobjects string `json:"ref-objects"`
}

type Export struct {
	Format               string       `json:"Format"`
	UserComment          string       `json:"UserComment"`
	AllFiles             string       `json:"AllFiles"`
	Persisted            string       `json:"Persisted"`
	IncludeInternalFiles string       `json:"IncludeInternalFiles,omitempty"`
	DeploymentPolicy     string       `json:"DeploymentPolicy,omitempty"`
	Domain               []DomainType `json:"Domain"`
}

type ExportRq struct {
	Export Export `json:"Export"`
}

type Import struct {
	InputFile              string       `json:"InputFile,omitempty"`
	Format                 string       `json:"Format,omitempty"`
	OverwriteFiles         string       `json:"OverwriteFiles,omitempty"`
	OverwriteObjects       string       `json:"OverwriteObjects,omitempty"`
	DryRun                 string       `json:"DryRun,omitempty"`
	RewriteLocalIP         string       `json:"RewriteLocalIP,omitempty"`
	DeploymentPolicy       string       `json:"DeploymentPolicy,omitempty"`
	DeploymentPolicyParams string       `json:"DeploymentPolicyParams,omitempty"`
	Object                 []ObjectType `json:"Object,omitempty"`
	File                   []FileType   `json:"File,omitempty"`
}

type ImportRq struct {
	Import Import `json:"Import,omitempty"`
}

type SaveConfig struct {
	SaveConfig string `json:"SaveConfig"`
}

type href struct {
	Href string `json:"href,omitempty"`
}

type link struct {
	Self     href `json:"self,omitempty"`
	Doc      href `json:"doc,omitempty"`
	Location href `json:"location,omitempty"`
}

type SaveConfigRs struct {
	Links struct {
		Doc struct {
			Href string `json:"href,omitempty"`
		} `json:"doc,omitempty"`
		Self struct {
			Href string `json:"href,omitempty"`
		} `json:"self,omitempty"`
	} `json:"_links,omitempty"`
	SaveConfig string `json:"SaveConfig,omitempty"`
	Scriptlog  string `json:"script-log,omitempty"`
}

type exportstatus struct {
	Neststatus string `json:"status"`
}

type ExportRs struct {
	Export exportstatus `json:"Export,omitempty"`
	Links  link         `json:"_links,omitempty"`
}

type BackupRs struct {
	Export struct {
		Status string `json:"status"`
	} `json:"Export"`
	Links struct {
		Location struct {
			Href string `json:"href"`
		} `json:"location"`
		Doc struct {
			Href string `json:"href"`
		} `json:"doc"`
		Self struct {
			Href string `json:"href"`
		} `json:"self"`
	} `json:"_links"`
}
type ImportRs struct {
	Import struct {
		Status string `json:"status"`
	} `json:"Import"`
	Links struct {
		Location struct {
			Href string `json:"href"`
		} `json:"location"`
		Doc struct {
			Href string `json:"href"`
		} `json:"doc"`
		Self struct {
			Href string `json:"href"`
		} `json:"self"`
	} `json:"_links"`
}
type RsDownload struct {
	Status string `json:"status"`
	Result struct {
		File string `json:"file"`
	} `json:"result"`
	Links struct {
		Self struct {
			Href string `json:"href"`
		} `json:"self"`
	} `json:"_links"`
}
type saveConfigRs struct {
	SaveConfig string `json:"SaveConfig"`
	Scriptlog  string `json:"script-log"`
	Links      struct {
		Doc struct {
			Href string `json:"href"`
		} `json:"doc"`
		Self struct {
			Href string `json:"href"`
		} `json:"self"`
	} `json:"_links"`
}
