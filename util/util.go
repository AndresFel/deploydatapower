package util

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"gitlab.com/AndresFel/deploydatapower/model"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

func EncodeBase64(pathfile string) string {
	// Open file on disk.
	f, _ := os.Open(pathfile)
	// Read entire JPG into byte slice.
	reader := bufio.NewReader(f)
	content, _ := ioutil.ReadAll(reader)
	// Encode as base64.
	encoded := base64.StdEncoding.EncodeToString(content)
	// Print encoded data to console.
	return encoded
}
func DecodeBase64ToFile(pathfile string, b64 string) {

	dec, err := base64.StdEncoding.DecodeString(b64)
	if err != nil {
		panic(err)
	}

	f, err := os.Create(pathfile)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	if _, err := f.Write(dec); err != nil {
		panic(err)
	}
	if err := f.Sync(); err != nil {
		panic(err)
	}
}
func Request(method string, url string, user string, pass string, body interface{}) (*http.Response, error) {
	//req, err := http.NewRequest("GET", "https://192.168.99.100:8001/mgmt/actionqueue/import/pending/Export-20200927T194018Z-3", nil)
	//req, err := http.NewRequest(method, url, bytes.NewBuffer(nil))

	//fmt.Printf("JSON Request\n")
	//fmt.Println(string(byteArray))
	var req *http.Request
	var errorRq error
	if method == "POST" {
		byteArray, err := json.Marshal(body)
		if err != nil {
			log.Fatal(err)
		}
		bufferBody := bytes.NewBuffer(byteArray)
		req, errorRq = http.NewRequest(method, url, bufferBody)
	} else {
		req, errorRq = http.NewRequest(method, url, nil)
	}

	if errorRq != nil {
		log.Fatal(errorRq)
	}
	req.SetBasicAuth("admin", "admin")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}
	//resp, err := client.Get("https://192.168.99.100:8001/mgmt/actionqueue/sandbox/operations")
	resp, err := client.Do(req)
	return resp, err
}
func Requestwithoutbody(method string, url string, user string, pass string) (*http.Response, error) {
	//req, err := http.NewRequest("GET", "https://192.168.99.100:8001/mgmt/actionqueue/import/pending/Export-20200927T194018Z-3", nil)
	//req, err := http.NewRequest(method, url, bytes.NewBuffer(nil))

	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.SetBasicAuth("admin", "admin")
	req.Header.Set("Accept", "application/json")
	//req.Header.Set("Content-Type", "application/json")

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}
	//resp, err := http.Get(url)
	resp, err := client.Do(req)
	return resp, err
}
func ImportDeployment(rutaDeployment string, url string, user string, pass string) {
	fielencodeDeployment := EncodeBase64(rutaDeployment)
	jsonImportObject := model.ImportRq{
		Import: model.Import{
			InputFile:        fielencodeDeployment,
			Format:           "XML",
			OverwriteFiles:   "on",
			OverwriteObjects: "on",
			DryRun:           "off",
			RewriteLocalIP:   "on",
			File: []model.FileType{
				{
					Name:      "deployment.xml",
					Overwrite: "on",
				},
			},
		},
	}

	response, _ := Request("POST", url, user, pass, jsonImportObject)
	if response.StatusCode == 202 {
		var jsonExportRs model.ImportRs
		data, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Fatal(err)
		}
		defer response.Body.Close()
		err2 := json.Unmarshal(data, &jsonExportRs)
		if err2 != nil {
			log.Fatal(err2)
		}
		fmt.Println("Status Import Deployment:\n", response.Status)
		fmt.Println("Response Import Deployment:\n", jsonExportRs)
		//Revisar si es necesario hacer el get con la respuesta del campo location que tiene la uri donde esta el estado final del proceso.
	} else {
		fmt.Println("Error Import Deployment:\n", response.Status)
	}
}
func ImportService(rutaService string, uriDeployment string, deploymentPolicy string, user string, pass string) {
	fielencodeDeployment := EncodeBase64(rutaService)
	jsonImportObject := model.ImportRq{
		Import: model.Import{
			InputFile:        fielencodeDeployment,
			Format:           "ZIP",
			OverwriteFiles:   "on",
			OverwriteObjects: "on",
			DryRun:           "off",
			RewriteLocalIP:   "on",
			DeploymentPolicy: deploymentPolicy,
			File: []model.FileType{
				{
					Name:      "service.zip",
					Overwrite: "on",
				},
			},
		},
	}

	response, _ := Request("POST", uriDeployment, user, pass, jsonImportObject)
	if response.StatusCode == 202 {
		fmt.Println("Status Import Service:\n", response.Status)
		var jsonExportRs model.ImportRs
		data, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Fatal(err)
		}
		defer response.Body.Close()
		err2 := json.Unmarshal(data, &jsonExportRs)
		if err2 != nil {
			log.Fatal(err2)
		}
		fmt.Println("Response Import Service:\n", jsonExportRs)
		//Revisar si es necesario hacer el get con la respuesta del campo location que tiene la uri donde esta el estado final del proceso.
	} else {
		fmt.Println("Error Import Service:\n", response.Status)
	}
}
func Backup(domain string, host string, user string, pass string) {
	jsonExportRq := model.ExportRq{
		Export: model.Export{
			Format:               "ZIP",
			UserComment:          "Export import domains",
			AllFiles:             "",
			Persisted:            "on",
			IncludeInternalFiles: "",
			DeploymentPolicy:     "",
			Domain: []model.DomainType{
				{
					Name:       domain,
					Refobjects: "on",
				},
			},
		},
	}
	resonse, _ := Request("POST", host+"/mgmt/actionqueue/default", user, pass, jsonExportRq)
	var jsonExportRs model.BackupRs
	data, err := ioutil.ReadAll(resonse.Body)
	if err != nil {
		log.Fatal(err)
	}
	defer resonse.Body.Close()
	err2 := json.Unmarshal(data, &jsonExportRs)
	if err2 != nil {
		log.Fatal(err2)
	}
	/*fmt.Println("Export Response:\n", jsonExportRs)
	fmt.Println("Status Code", resonse.Status)
	fmt.Println("URI Download", "https://192.168.99.100:8001"+jsonExportRs.Links.Location.Href)*/
	if resonse.StatusCode == 202 {
		fmt.Println("Status Backup Domain:\n", resonse.Status)
		downloadBackup(host+jsonExportRs.Links.Location.Href, domain, user, pass)
	} else {
		fmt.Println("Error Backup Domain:\n", resonse.Status)
	}
}

func downloadBackup(url string, domain string, user string, pass string) {
	time.Sleep(5000 * time.Millisecond)
	downloadexport, _ := Request("GET", url, user, pass, nil)
	var downloadrs model.RsDownload
	defer downloadexport.Body.Close()
	//json.NewDecoder(downloadexport.Body).Decode(downloadrs)
	//json.NewDecoder(r.Body).Decode(target)
	//fmt.Println("downloadexport Response:\n", downloadrs)
	if downloadexport.StatusCode == 200 {
		fmt.Println("Status Download Backup\n", downloadexport.Status)
		datadownload, err := ioutil.ReadAll(downloadexport.Body)
		if err != nil {
			log.Fatal(err)
		}
		defer downloadexport.Body.Close()
		err3 := json.Unmarshal(datadownload, &downloadrs)
		if err3 != nil {
			log.Fatal(err3)
		}
		DecodeBase64ToFile("C:/Users/Abustos/Documents/deploydatapower/"+domain+"-"+time.Now().Format("2006-01-02")+".zip", downloadrs.Result.File)
	} else {
		fmt.Println("Error Download Backup\n", downloadexport.Status)
	}
}

func SaveConfig(url string, user string, pass string) {
	saveconfig := model.SaveConfig{
		SaveConfig: "0",
	}
	response, _ := Request("POST", url, user, pass, saveconfig)
	if response.StatusCode == 200 {
		fmt.Println("Status Save Configuration:\n", response.Status)
		var jsonsaveConfigRs model.SaveConfigRs
		data, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Fatal(err)
		}
		defer response.Body.Close()
		err2 := json.Unmarshal(data, &jsonsaveConfigRs)
		if err2 != nil {
			log.Fatal(err2)
		}
		fmt.Println("Response Save Configuration:\n", jsonsaveConfigRs)
	} else {
		fmt.Println("Error Save Configuration:\n", response.Status)
	}
}
